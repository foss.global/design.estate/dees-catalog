import { tap, expect, webhelpers } from '@push.rocks/tapbundle';

import * as deesCatalog from '../ts_web';

tap.test('should create a working button', async () => {
  const button: deesCatalog.DeesButton = await webhelpers.fixture(
    webhelpers.html`<dees-button></dees-button>`
  );
  expect(button).toBeInstanceOf(deesCatalog.DeesButton);
});

tap.start();
