/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@design.estate/dees-catalog',
  version: '1.0.173',
  description: 'website for lossless.com'
}
