import {
  customElement,
  DeesElement,
  type TemplateResult,
  property,
  html,
  css,
  cssManager,
  type CSSResult,
} from '@design.estate/dees-element';
import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-input-checkbox': DeesInputCheckbox;
  }
}

@customElement('dees-input-checkbox')
export class DeesInputCheckbox extends DeesElement {
  // STATIC
  public static demo = () => html`<dees-input-checkbox></dees-input-checkbox>`;

  // INSTANCE
  public changeSubject = new domtools.rxjs.Subject();

  @property({
    type: String,
  })
  public key: string;

  @property({
    type: String,
  })
  public label: string = 'Label';

  @property({
    type: Boolean,
  })
  public value: boolean = false;

  @property({
    type: Boolean,
  })
  public required: boolean = false;

  @property({
    type: Boolean
  })
  public disabled: boolean = false;

  public render(): TemplateResult {
    return html`
      ${domtools.elementBasic.styles}
      <style>
        * {
          box-sizing: border-box;
        }

        :host {
          display: block;
          position: relative;
          margin: 20px 0px;
          cursor: pointer;
        }

        .maincontainer {
          display: grid;
          grid-template-columns: 25px auto;
          padding: 5px 0px;
          color: ${this.goBright ? '#333' : '#ccc'};
        }

        .maincontainer:hover {
          ${this.goBright ? '#000' : '#ccc'};
        }

        .label {
          margin-left: 15px;
          line-height: 25px;
          font-size: 14px;
          font-weight: normal;
        }

        input:focus {
          outline: none;
          border-bottom: 1px solid #e4002b;
        }

        .checkbox {
          transition: all 0.1s;
          box-sizing: border-box;
          border: 1px solid ${this.goBright ? '#CCC' : '#999'};
          border-radius: 2px;
          height: 24px;
          width: 24px;
          display: inline-block;
          background: ${this.goBright ? '#fafafa' : '#222'};
        }

        .checkbox.selected {
          background: #039BE5;
          border: 1px solid #039BE5;
        }

        .checkbox.disabled {
          background: none;
          border: 1px dashed ${cssManager.bdTheme('#666666', '#666666')};
        }

        .checkbox .checkmark {
          display: inline-block;
          width: 22px;
          height: 22px;
          -ms-transform: rotate(45deg); /* IE 9 */
          -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
          transform: rotate(45deg);
        }

        .checkbox .checkmark_stem {
          position: absolute;
          width: 3px;
          height: 9px;
          background-color: #fff;
          left: 11px;
          top: 6px;
        }

        .checkbox .checkmark_kick {
          position: absolute;
          width: 3px;
          height: 3px;
          background-color: #fff;
          left: 8px;
          top: 12px;
        }

        .checkbox.disabled .checkmark_stem, .checkbox.disabled .checkmark_kick {
          background-color: ${cssManager.bdTheme('#333', '#fff')};
        }

        img {
          padding: 4px;
        }
      </style>
      <div class="maincontainer" @click="${this.toggleSelected}">
        <div class="checkbox ${this.value ? 'selected' : ''} ${this.disabled ? 'disabled' : ''}">
          ${this.value
            ? html`
                <span class="checkmark">
                  <div class="checkmark_stem"></div>
                  <div class="checkmark_kick"></div>
                </span>
              `
            : html``}
        </div>
        <div class="label">${this.label}</div>
      </div>
    `;
  }

  public async toggleSelected() {
    if (this.disabled) {
      return;
    }
    this.value = !this.value;
    this.dispatchEvent(
      new CustomEvent('newValue', {
        detail: this.value,
        bubbles: true,
      })
    );
    this.changeSubject.next(this);
  }
}
