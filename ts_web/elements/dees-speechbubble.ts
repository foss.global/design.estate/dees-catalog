import {
  customElement,
  html,
  DeesElement,
  property,
  type TemplateResult,
  cssManager,
  css,
  type CSSResult,
  unsafeCSS,
} from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-speechbubble': DeesSpeechbubble;
  }
}

@customElement('dees-speechbubble')
export class DeesSpeechbubble extends DeesElement {
  public static demo = () => html` <dees-speechbubble></dees-speechbubble> `;

  @property()
  public text: string;

  @property({
    type: Boolean,
  })
  public disabled = false;

  @property({
    type: Boolean,
  })
  public isHidden = false;

  @property({
    type: String,
  })
  public status: 'normal' | 'pending' | 'success' | 'error' = 'normal';

  constructor() {
    super();
  }

  public static styles = [
    cssManager.defaultStyles,
    css`
      :host {
        position: relative;
        display: block;
        box-sizing: border-box;
        color: ${cssManager.bdTheme('#333', '#fff')};
        cursor: pointer;
        user-select: none;
      }
      :host([hidden]) {
        display: none;
      }

      .maincontainer {
        will-change: transform;
        transition: transform 0.2s;
        transform: translateX(0px);
        position: relative;
        transition: all 0.2s;
        margin-left: 0px;
      }

      .maincontainer:hover {
        transform: translateX(3px);
      }

      .arrow {
        position: absolute;
        transform: rotate(45deg);
        background: ${cssManager.bdTheme('#fff', '#333')};
        height: 15px;
        width: 15px;
        left: 4px;
        top: 5px;
        border-radius: 2px;
      }

      .speechbubble {
        background: ${cssManager.bdTheme('#fff', '#333')};
        padding: 0px 10px;
        border-radius: 3px;
        position: absolute;
        line-height: 25px;
        font-size: 12px;
        top: 0px;
        left: 8px;
      }

      .wave {
        animation-name: wave-animation; /* Refers to the name of your @keyframes element below */
        animation-duration: 2.5s; /* Change to speed up or slow down */
        animation-iteration-count: infinite; /* Never stop waving :) */
        transform-origin: 70% 70%; /* Pivot around the bottom-left palm */
        display: inline-block;
      }

      @keyframes wave-animation {
        0% {
          transform: rotate(0deg);
        }
        10% {
          transform: rotate(14deg);
        } /* The following five values can be played with to make the waving more or less extreme */
        20% {
          transform: rotate(-8deg);
        }
        30% {
          transform: rotate(14deg);
        }
        40% {
          transform: rotate(-4deg);
        }
        50% {
          transform: rotate(10deg);
        }
        60% {
          transform: rotate(0deg);
        } /* Reset for the last half to pause */
        100% {
          transform: rotate(0deg);
        }
      }
    `,
  ];

  public render(): TemplateResult {
    return html`
      <div class="maincontainer" @click=${this.handleClick}>
        <div class="arrow"></div>
        <div class="speechbubble"><span class="wave">👋</span> We build with launch.sh, and you can too.</div>
      </div>
    `;
  }

  public async handleClick() {
    if (this.disabled) {
      return;
    }

    globalThis.location.href = "https://launch.sh"
  }

  public async firstUpdated() {
    if (!this.textContent) {
      this.textContent = 'Button';
      this.performUpdate();
    }
  }
}
