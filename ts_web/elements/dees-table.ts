import {
  customElement,
  html,
  DeesElement,
  property,
  type TemplateResult,
  cssManager,
  css,
  unsafeCSS,
  type CSSResult,
  state,
} from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-table': DeesTable<any>;
  }
}

export interface IDataAction<T = any> {
  name: string;
  iconName: string;
  useTableBehaviour?: 'upload' | 'cancelUpload' | 'none';
  actionFunc: (itemArg: T) => Promise<any>;
}

@customElement('dees-table')
export class DeesTable<T> extends DeesElement {
  public static demo = () => html`
    <style>
      .demoWrapper {
        box-sizing: border-box;
        position: absolute;
        width: 100%;
        height: 100%;
        padding: 60px;
        background: #000000;
      }
    </style>
    <div class="demoWrapper">
      <dees-table
        heading1="Current Account Statement"
        heading2="Bunq - Payment Account 2 - April 2021"
        .data=${[
          {
            date: '2021-04-01',
            amount: '2464.65 €',
            description: 'Printing Paper (Office Supplies) - STAPLES BREMEN',
          },
          {
            date: '2021-04-02',
            amount: '165.65 €',
            description: 'Logitech Mouse (Hardware) - logi.com OnlineShop',
          },
          {
            date: '2021-04-03',
            amount: '2999,00 €',
            description: 'Macbook Pro 16inch (Hardware) - Apple.de OnlineShop',
          },
          {
            date: '2021-04-01',
            amount: '2464.65 €',
            description: 'Office-Supplies - STAPLES BREMEN',
          },
          {
            date: '2021-04-01',
            amount: '2464.65 €',
            description: 'Office-Supplies - STAPLES BREMEN',
          },
        ]}
        .dataActions="${[{
          name: 'upload',
          iconName: 'upload',
          useTableBehaviour: 'upload',
          actionFunc: async (itemArg: any) => {

          },
        },{
          name: 'visibility',
          iconName: 'visibility',
          useTableBehaviour: 'preview',
          actionFunc: async (itemArg: any) => {

          },
        }] as IDataAction[]}"
        >This is a slotted Text</dees-table>
    </div>
  `;

  // INSTANCE
  @property({
    type: String,
  })
  public heading1: string = 'heading 1';

  @property({
    type: String,
  })
  public heading2: string = 'heading 2';

  @property({
    type: Array,
  })
  public data: T[] = [];

  @property({
    type: Array,
  })
  public dataActions: IDataAction[] = [];

  @property({
    type: Object,
  })
  public selectedDataRow: T;

  @property({
    type: String,
  })
  public type: 'normal' | 'highlighted' | 'discreet' | 'big' = 'normal';

  @property({
    type: String,
  })
  public status: 'normal' | 'pending' | 'success' | 'error' = 'normal';

  public files: File[] = [];
  public fileWeakMap = new WeakMap();

  constructor() {
    super();
  }

  public static styles = [
    cssManager.defaultStyles,
    css`
      .mainbox {
        color: ${cssManager.bdTheme('#333', '#fff')};
        font-family: 'Mona Sans', 'Inter', sans-serif;
        font-weight: 400;
        font-size: 16px;
        padding: 16px;
        display: block;
        width: 100%;
        min-height: 50px;
        background: ${cssManager.bdTheme('#ffffff', '#333333')};
        border-radius: 3px;
        border-top: 1px solid ${cssManager.bdTheme('#fff', '#444')};
        box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.3);
        overflow-x: auto;
      }
      .headingSeparation {
        margin-top: 7px;
        border-bottom: 1px solid ${cssManager.bdTheme('#bcbcbc', '#bcbcbc')};
      }

      table,
      .noDataSet {
        margin-top: 16px;
        color: ${cssManager.bdTheme('#333', '#fff')};
        border-collapse: collapse;
        width: 100%;
      }
      .noDataSet {
        text-align: center;
      }
      tr {
        border-bottom: 1px dashed ${cssManager.bdTheme('#999', '#808080')};
        text-align: left;
      }
      tr:last-child {
        border-bottom: none;
        text-align: left;
      }
      tr:hover {
        cursor: pointer;
      }
      tr:hover .innerCellContainer {
        background: ${cssManager.bdTheme('#22222210', '#ffffff20')};
      }
      tr:first-child:hover {
        cursor: auto;
      }
      tr:first-child:hover .innerCellContainer {
        background: none;
      }
      tr.selected .innerCellContainer {
        background: ${cssManager.bdTheme('#22222220', '#ffffff20')};
      }
      th {
        text-transform: uppercase;
      }
      th,
      td {
        padding: 3px 0px;
        border-right: 1px dashed ${cssManager.bdTheme('#999', '#808080')};
      }
      .innerCellContainer {
        padding: 6px 8px;
      }
      th:first-child .innerCellContainer,
      td:first-child .innerCellContainer {
        padding-left: 0px;
      }
      th:last-child .innerCellContainer,
      td:last-child .innerCellContainer {
        padding-right: 0px;
      }
      th:last-child,
      td:last-child {
        border-right: none;
      }

      .action {
        margin: -8px 0px;
        padding: 8px;
        line-height: 16px;
        display: inline-block;
      }

      .action:first-child {
        margin-left: -8px;
        width: min-content;
      }

      .action:hover {
        background: ${cssManager.bdTheme('#CCC', '#111')};
      }

      .tableStatistics {
        padding: 4px 16px;
        font-size: 12px;
        color: ${cssManager.bdTheme('#111', '#ffffff90')};
        background: ${cssManager.bdTheme('#eeeeeb', '#00000050')};
        margin: 16px -16px -16px -16px;
        border-bottom-left-radius: 3px;
        border-bottom-right-radius: 3px;
      }
    `,
  ];

  public render(): TemplateResult {
    return html`
      <div class="mainbox">
        <!-- the heading part -->
        <div>${this.heading1}</div>
        <div>${this.heading2}</div>
        <div class="headingSeparation"></div>

        <!-- the actual table -->
        <style></style>
        ${this.data.length > 0
          ? (() => {
              const headings: string[] = Object.keys(this.data[0]);
              return html`
                <table>
                  <tr>
                    ${headings.map(
                      (headingArg) => html`
                        <th>
                          <div class="innerCellContainer">${headingArg}</div>
                        </th>
                      `
                    )}
                    ${(() => {
                      if (this.dataActions && this.dataActions.length > 0) {
                        return html`
                          <th>
                            <div class="innerCellContainer">Actions</div>
                          </th>
                        `;
                      }
                    })()}
                  </tr>
                  ${this.data.map(
                    (itemArg) => html`
                      <tr
                        @click=${() => {
                          this.selectedDataRow = itemArg;
                        }}
                        @dragenter=${async (eventArg: DragEvent) => {
                          console.log((eventArg.target as HTMLElement).tagName)
                          console.log('dragenter');
                          eventArg.preventDefault();
                          eventArg.stopPropagation();
                          (eventArg.target as HTMLElement).parentElement.style.background = '#800000';
                        }}
                        @dragleave=${async (eventArg: DragEvent) => {
                          console.log((eventArg.target as HTMLElement).tagName)
                          console.log('dragleave');
                          eventArg.preventDefault();
                          eventArg.stopPropagation();
                          (eventArg.target as HTMLElement).parentElement.style.background = 'none';
                        }}
                        @dragover=${async (eventArg: DragEvent) => {
                          eventArg.preventDefault();
                        }}
                        @drop=${async (eventArg: DragEvent) => {
                          eventArg.preventDefault();
                          const newFiles = []
                          for (const file of Array.from(eventArg.dataTransfer.files)) {
                            this.files.push(file);
                            newFiles.push(file);
                            this.requestUpdate();
                          }
                          const result: File[] = this.fileWeakMap.get(itemArg as object);
                          if (!result) {
                            this.fileWeakMap.set(itemArg as object, newFiles)
                          } else {
                            result.push(...newFiles);
                          }
                        }}
                        class="${itemArg === this.selectedDataRow ? 'selected' : ''}"
                      >
                        ${headings.map(
                          (headingArg) => html`
                            <td>
                              <div class="innerCellContainer">${itemArg[headingArg]}</div>
                            </td>
                          `
                        )}
                        ${(() => {
                          if (this.dataActions && this.dataActions.length > 0) {
                            return html`
                              <td>
                                <div class="innerCellContainer">
                                  ${(() => {
                                    const actions: TemplateResult[] = [];
                                    for (const action of this.dataActions) {
                                      actions.push(html`<div class="action">${action.iconName ? html`
                                        <dees-icon .iconName=${'upload_file'}></dees-icon>
                                      ` : action.name}</div>`)
                                    }
                                    return actions;
                                  })()}
                                </div>
                              </td>
                            `;
                          }
                        })()}
                      </tr>
                    `
                  )}
                </table>
              `;
            })()
          : html` <div class="noDataSet">No data set!</div> `}
        <div class="tableStatistics">
          ${this.data.length} data rows (total) |
          ${this.selectedDataRow
            ? html`Row ${this.data.indexOf(this.selectedDataRow) + 1} selected`
            : html`No row selected`}
        </div>
      </div>
    `;
  }

  public async firstUpdated() {}
}
