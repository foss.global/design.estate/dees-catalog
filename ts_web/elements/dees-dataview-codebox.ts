import {
  DeesElement,
  html,
  customElement,
  type TemplateResult,
  property,
  state,
} from '@design.estate/dees-element';

import hlight from 'highlight.js';

import * as smartstring from '@push.rocks/smartstring';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-dataview-codebox': DeesDataviewCodebox;
  }
}

@customElement('dees-dataview-codebox')
export class DeesDataviewCodebox extends DeesElement {
  public static demo = () => html`<dees-dataview-codebox proglang="typescript">
    import * as text from './hello'; const hiThere = 'nice'; const myFunction = async () => {
    console.log('nice one'); }
  </dees-dataview-codebox>`;

  @property()
  public progLang: string = 'typescript';

  @property({
    type: String,
    reflect: true,
  })
  public codeToDisplay: string = '';

  constructor() {
    super();
  }

  render(): TemplateResult {
    return html`
      ${domtools.elementBasic.styles}
      <style>
        :host {
          position: relative;
          display: block;
          text-align: left;
          font-size: 16px;
        }
        .mainbox {
          position: relative;
          color: ${this.goBright ? '#333333' : '#ffffff'};
        }

        .languageLabel {
          color: #fff;
          font-size: 12px;
          z-index: 10;
          background: #6596ff;
          display: inline-block;
          position: absolute;
          right: 32px;
          padding: 4px;
          border-bottom-left-radius: 3px;
          border-bottom-right-radius: 3px;
        }

        .hljs-keyword {
          color: #ff65ec;
        }

        .codegrid {
          display: grid;
          grid-template-columns: 50px auto;
          background: ${this.goBright ? '#ffffff' : '#191919'};
          border-top: 1px solid ${this.goBright ? '#ffffff' : '#333333'};
          box-shadow: 0px 0px 5px ${this.goBright ? 'rgba(0,0,0,0.1)' : 'rgba(0,0,0,0.5)'};
          border-radius: 3px;
          overflow: hidden;
        }

        .lineNumbers {
          background: ${this.goBright ? '#fafafa' : '#151515'};
          color: ${this.goBright ? '#acacac' : '#666666'};
          padding: 30px 16px 0px 0px;
          text-align: right;
        }

        .lineCounter:last-child {
          opacity: 50%;
        }

        pre {
          overflow-x: auto;
          margin: 0px;
          padding: 30px 40px;
        }

        code {
          font-weight: ${this.goBright ? '400' : '300'};
          padding: 0px;
          margin: 0px;
        }

        code,
        code *,
        .lineNumbers {
          line-height: 1.4em;
          font-weight: 200;
          font-family: 'Intel One Mono', 'monospace';
        }

        .hljs-string {
          color: #ffa465;
        }

        .hljs-built_in {
          color: #65ff6a;
        }

        .hljs-function {
          color: ${this.goBright ? '#2765DF': '#6596ff' };
        }

        .hljs-params {
          color: ${this.goBright ? '#3DB420' : '#65d5ff' };
        }

        .hljs-comment {
          color: ${this.goBright ? '#EF9300' : '#ffd765' };
        }
      </style>
      <div class="mainbox">
        <div class="languageLabel">${this.progLang}</div>
        <div class="codegrid">
          <div class="lineNumbers">
            ${(() => {
              let lineCounter = 0;
              return this.codeToDisplay.split('\n').map(lineArg => {
                lineCounter++;
                return html`<div class="lineCounter">${lineCounter}</div>`;
              })
            })()}
          </div>
          <pre><code></code></pre>
        </div>
      </div>
    `;
  }

  @state()
  private codeToDisplayStore = '';

  public async updated(_changedProperties) {
    super.updated(_changedProperties);
    console.log('highlighting now');
    console.log(this.childNodes);
    const slottedCodeNodes: Text[] = [];
    this.childNodes.forEach((childNode) => {
      if (childNode.nodeName === '#text') {
        slottedCodeNodes.push(childNode as Text);
      }
    });
    if (this.codeToDisplay && this.codeToDisplay !== this.codeToDisplayStore) {
      this.codeToDisplayStore = smartstring.indent.normalize(this.codeToDisplay).trimStart();
    }
    if (slottedCodeNodes[0] && slottedCodeNodes[0].wholeText && !this.codeToDisplay) {
      this.codeToDisplayStore = smartstring.indent.normalize(slottedCodeNodes[0].wholeText).trimStart();
      this.codeToDisplay = this.codeToDisplayStore;
    }
    await domtools.plugins.smartdelay.delayFor(0);
    const localCodeNode = this.shadowRoot.querySelector('code');
    const html = hlight.highlight(this.codeToDisplayStore, {language: this.progLang, ignoreIllegals: true});
    localCodeNode.innerHTML = html.value;
  }
}
