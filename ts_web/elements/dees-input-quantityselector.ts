import { customElement, property, html, type TemplateResult, DeesElement, type CSSResult, } from '@design.estate/dees-element';
import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-input-quantityselector': DeesInputQuantitySelector;
  }
}

@customElement('dees-input-quantityselector')
export class DeesInputQuantitySelector extends DeesElement {
  public static demo = () => html`<dees-input-quantityselector></dees-input-quantityselector>`;

  // INSTANCE
  public changeSubject = new domtools.rxjs.Subject();

  @property()
  public key: string;

  @property({
    type: Number
  })
  public value: number = 1;

  @property({
    type: Boolean,
  })
  public required: boolean = false;

  @property({
    type: Boolean
  })
  public disabled: boolean = false;

  constructor() {
    super();
  }

  public render(): TemplateResult {
    return html`
      ${domtools.elementBasic.styles}
      <style>
        :host {
          display: block;
          width: 110px;
          user-select: none;
        }
        .maincontainer {
          transition: all 0.1s;
          font-size: 14px;
          display: grid;
          grid-template-columns: 33% 34% 33%;
          text-align: center;
          background:none;
          line-height: 40px;
          padding: 0px;
          min-width: 100px;
          color: ${this.goBright ? '#666' : '#CCC'};
          border: ${this.goBright ? '1px solid #333' : '1px solid #CCC'};
          border-radius: 4px;
        }

        .mainContainer:hover {
          color: ${this.goBright ? '#333' : '#fff'};
          border: ${this.goBright ? '1px solid #333' : '1px solid #fff'};
        }

        .minus {
          padding-left: 5px;
        }

        .plus {
          padding-right: 5px;
        }

        .selector {
          text-align: center;
          font-size: 20px;
        }

        .selector:hover {
          cursor: pointer;
        }

        .quantity {
          text-align: center;
        }

        
      </style>

      <div class="maincontainer">
        <div class="selector minus" @click="${() => {this.decrease();}}">-</div>
        <div class="quantity">${this.value}</div>
        <div class="selector plus" @click="${() => {this.increase();}}">+</div>
      </div>
    `;
  }

  public increase () {
    this.value++;
    this.changeSubject.next(this);
  }

  public decrease () {
    if (this.value > 0) {
      this.value--;
    } else {
      // nothing to do here
    }
    this.changeSubject.next(this);
  }
}
