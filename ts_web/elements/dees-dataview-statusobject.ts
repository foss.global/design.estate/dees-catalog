import {
  DeesElement,
  html,
  customElement,
  type TemplateResult,
  property,
  state,
  cssManager,
  css,
  type CSSResult,
} from '@design.estate/dees-element';

import * as tsclass from '@tsclass/tsclass';

declare global {
  interface HTMLElementTagNameMap {
    'dees-dataview-statusobject': DeesDataviewStatusobject;
  }
}

@customElement('dees-dataview-statusobject')
export class DeesDataviewStatusobject extends DeesElement {
  public static demo = () => html`<dees-dataview-statusobject
    .statusObject=${{
      id: '1',
      name: 'Demo Item',
      combinedStatus: 'partly_ok',
      combinedStatusText: 'partly_ok',
      details: [
        {
          name: 'Detail 1',
          value: 'Value 1',
          status: 'ok',
          statusText: 'OK',
        },
        {
          name: 'Detail 2',
          value: 'Value 2',
          status: 'partly_ok',
          statusText: 'partly_ok',
        },
        {
          name: 'Detail 3',
          value: 'Value 3',
          status: 'not_ok',
          statusText: 'not_ok',
        },
        {
          name: 'Detail 4',
          value: 'Value 4 jhdkfjhalskdfjhfdjskalsdkfjhfdjskalskdjfhjdkslaksjdhfjdkslaskdfjhfjdkslaskdjfhjdskalskdjhfdjskalskdjfhdjskl',
          status: 'ok',
          statusText: 'OK',
        },
      ],
    }}
  >
  </dees-dataview-statusobject>`;

  @property({ type: Object }) statusObject: tsclass.code.IStatusObject;

  public static styles = [
    cssManager.defaultStyles,
    css`
      .mainbox {
        border-radius: 3px;
        background: ${cssManager.bdTheme('#fff', '#1b1b1b')};
        box-shadow: 0px 1px 3px #00000030;
        min-height: 48px;
        color: ${cssManager.bdTheme('#000', '#fff')};
      }

      .heading {
        display: grid;
        align-items: center;
        grid-template-columns: 40px auto 120px;
      }

      h1 {
        display: block;
        margin: 0px;
        padding: 0px;
        height: 48px;
        text-transform: uppercase;
        font-size: 12px;
        line-height: 48px;
      }

      .statusdot {
        height: 8px;
        width: 8px;
        border-radius: 6px;
        background: grey;
        margin: auto;
      }

      .copyMain {
        cursor: pointer;
        font-size: 8px;
        font-weight: 600;
        text-transform: uppercase;
        border: 1px solid ${cssManager.bdTheme('#999', '#444')};
        text-align: center;
        padding: 4px;
        border-radius: 3px;
        margin-right: 16px;
      }

      .statusdot.ok {
        background: green;
      }

      .statusdot.not_ok{
        background: red;
      }

      .statusdot.partly_ok {
        background: orange;
      }

      .detail {
        minheight: 60px;
        align-items: center;
        display: grid;
        grid-template-columns: 40px auto;
        border-top: 1px dotted ${cssManager.bdTheme('#999', '#444')};
      }

      .detail .detailsText {
        padding-top: 8px;
        padding-bottom: 8px;
        padding-right: 8px;
        word-break: break-all;
      }

      .detail .detailsText .label {
        font-size: 12px;
      }

      .detail .detailsText .value {
        font-size: 16px;
        font-family: 'Intel One Mono';
      }
    `,
  ];

  render(): TemplateResult {
    return html`
      <div class="mainbox">
        <div class="heading">
          <div class="statusdot ${this.statusObject?.combinedStatus}"></div>
          <h1>${this.statusObject?.name || 'no status object assigned'}</h1>
          <div class="copyMain">Copy as JSON</div>
        </div>
        ${this.statusObject?.details?.map((detailArg) => {
          return html`
            <div class="detail">
              <div class="statusdot ${detailArg.status}"></div>
              <div class="detailsText">
                <div class="label">${detailArg.name}</div>
                <div class="value">${detailArg.value}</div>
              </div>
            </div>
          `;
        })}
      </div>
    `;
  }

  async firstUpdated() {}
}
