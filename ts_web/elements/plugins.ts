// @push.rocks scope
import * as smartpromise from '@push.rocks/smartpromise';

export {
  smartpromise,
}

// @tsclass scope
import * as tsclass from '@tsclass/tsclass';

export {
  tsclass
}
