import {
  customElement,
  html,
  DeesElement,
  property,
  type TemplateResult,
  cssManager,
  css,
  type CSSResult,
  unsafeCSS,
} from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-chips': DeesChips;
  }
}

@customElement('dees-chips')
export class DeesChips extends DeesElement {
  public static demo = () => html`
    <dees-chips .selectableChips=${['Payment Account 1', 'PaymentAccount2', 'Payment Account 3']}></dees-chips>
    <dees-chips selectionMode="multiple" .selectableChips=${['Payment Account 1', 'PaymentAccount2', 'Payment Account 3']}></dees-chips>
    
  `;

  @property()
  public selectionMode: 'single' | 'multiple' = 'single';

  @property({
    type: Array
  })
  public selectableChips: string[] = [];

  @property()
  public selectedChip: string = null;

  @property({
    type: Array
  })
  public selectedChips: string[] = [];
  

  constructor() {
    super();
  }

  public static styles = [
    cssManager.defaultStyles,
    css`

      :host {
        display: block;
        box-sizing: border-box;
      }

      .mainbox {

      }

      .chip {
        background: #494949;
        display: inline-block;
        padding: 8px 12px;
        font-size: 14px;
        color: #fff;
        border-radius: 40px;
        margin-right: 4px;
        margin-bottom: 8px;
      }

      .chip:hover {
        background: #666666;
        cursor: pointer; 
      }

      .chip.selected {
        background: #00A3FF;
      }
      
    `,
  ];

  public render(): TemplateResult {
    return html`
      <div class="mainbox">
        ${this.selectableChips.map(chipArg => html`
          <div @click=${() => this.selectChip(chipArg)} class="chip ${this.selectedChip === chipArg || this.selectedChips.includes(chipArg) ? 'selected' : ''}">
            ${chipArg}
          </div>
        `)}
      </div>
    `;
  }

  public async firstUpdated() {
    if (!this.textContent) {
      this.textContent = 'Button';
      this.performUpdate();
    }
  }

  public async selectChip(chipArg: string) {
    if (this.selectionMode === 'single') {
      if (this.selectedChip === chipArg) {
        this.selectedChip = null;
        this.selectedChips = [];
      } else {
        this.selectedChip = chipArg;
        this.selectedChips = [chipArg];
      }
    } else if(this.selectionMode === 'multiple') {
      if (this.selectedChips.includes(chipArg)) {
        this.selectedChips = this.selectedChips.filter(chipArg2 => chipArg !== chipArg2)
      } else {
        this.selectedChips.push(chipArg);
      }
      this.requestUpdate();
    }
    console.log(this.selectedChips);
  }
}
