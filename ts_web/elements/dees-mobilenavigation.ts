import * as plugins from './plugins.js';
import {
  cssManager,
  css,
  type CSSResult,
  customElement,
  DeesElement,
  domtools,
  html,
  property,
} from '@design.estate/dees-element';
import { DeesWindowLayer } from './dees-windowlayer.js';

@customElement('dees-mobilenavigation')
export class DeesMobilenavigation extends DeesElement {
  // STATIC
  public static demo = () => html`
    <dees-mobilenavigation
      .menuItems="${[
        {
          name: 'hello1',
          action: async () => {},
        },
        {
          name: 'hello2',
          action: async () => {},
        },
        {
          name: 'hello3',
          action: async () => {},
        },
      ]}"
    ></dees-mobilenavigation>
  `;

  private static singletonRef: DeesMobilenavigation;
  public static async createAndInit(menuItemsArg: plugins.tsclass.website.IMenuItem[]) {
    if (!this.singletonRef) {
      this.singletonRef = new DeesMobilenavigation();
      document.body.append(this.singletonRef);
      await this.singletonRef.init();
    }
    this.singletonRef.menuItems = menuItemsArg;
    await this.singletonRef.readyDeferred.promise;
    this.singletonRef.show();
    return this.singletonRef;
  }

  // INSTANCE
  @property({
    type: Array,
  })
  public heading: string = `MENU`;

  @property({
    type: Array,
  })
  public menuItems: plugins.tsclass.website.IMenuItem[] = [];

  readyDeferred: plugins.smartpromise.Deferred<any> = domtools.plugins.smartpromise.defer();

  constructor() {
    super();
    /* this.init().then(() => {
      this.show();
    }); */
  }

  /**
   * inits the mobile navigation
   */
  public async init() {
    await this.updateComplete;
    this.readyDeferred.resolve();
  }

  public static styles = [
    cssManager.defaultStyles,
    css`
      :host {
      }

      .main {
        transition: all 0.3s cubic-bezier(0.22, 1, 0.36, 1);
        will-change: transform;
        position: fixed;
        height: 100vh;
        min-width: 280px;
        transform: translateX(200px);
        color: ${cssManager.bdTheme('#333', '#fff')};
        z-index: 250;
        opacity: 0;
        padding: 16px 32px;
        right: 0px;
        top: 0px;
        bottom: 0px;
        background: ${cssManager.bdTheme('#eeeeeb', '#000')};;
        border-left: 1px dashed #444;
        pointer-events: none;
      }

      .main.show {
        pointer-events: all;
        transform: translateX(0px);
        opacity: 1;
      }

      .menuItem {
        text-align: left;
        padding: 8px;
        margin-left: -8px;
        margin-right: -8px;
        cursor: pointer;
        border-radius: 3px;
      }
      .menuItem:hover {
        background: ${cssManager.bdTheme('#CCC', '#333')};;
      }

      .heading {
        text-align: left;
        font-size: 24px;
        padding: 8px 0px;
        font-family: 'Mona Sans', 'Inter', sans-serif;
        font-weight: 300;
        border-bottom: 1px dashed #444;
        margin-top: 16px;
        margin-bottom: 16px;
      }
    `,
  ];

  public render() {
    return html`
      <div class="main">
        <div class="heading">${this.heading}</div>
        ${this.menuItems.map((menuItem) => {
          return html`
            <div
              class="menuItem"
              @click="${() => {
                this.hide();
                menuItem.action();
              }}"
            >
              ${menuItem.name}
            </div>
          `;
        })}
      </div>
    `;
  }

  private windowLayer: DeesWindowLayer;

  /**
   * inits the show
   */
  public async show() {
    const domtools = await this.domtoolsPromise;
    const main = this.shadowRoot.querySelector('.main');
    if (!this.windowLayer) {
      this.windowLayer = new DeesWindowLayer();
      this.windowLayer.addEventListener('click', () => {
        this.hide();
      });
    }
    document.body.append(this.windowLayer);
    await domtools.convenience.smartdelay.delayFor(0);
    this.windowLayer.show();

    await domtools.convenience.smartdelay.delayFor(0);
    main.classList.add('show');
  }

  /**
   * inits the hide function
   */
  public async hide() {
    const domtools = await this.domtoolsPromise;
    const main = this.shadowRoot.querySelector('.main');
    main.classList.remove('show');
    this.windowLayer.hide();
  }

  async disconnectedCallback() {
    document.body.removeChild(this.windowLayer);
  }
}
