import { customElement, DeesElement, type TemplateResult, html, type CSSResult, } from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-toast': DeesToast;
  }
}

@customElement('dees-toast')
export class DeesToast extends DeesElement {

  constructor() {
    super();
    domtools.elementBasic.setup();
  }

  public render(): TemplateResult {
    return html`
      ${domtools.elementBasic.styles}
      <style></style>
      
    `;
  }
}
