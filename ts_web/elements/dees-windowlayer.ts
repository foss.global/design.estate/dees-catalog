import { customElement, DeesElement, type TemplateResult, html, property, type CSSResult, } from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-windowlayer': DeesWindowLayer;
  }
}

@customElement('dees-windowlayer')
export class DeesWindowLayer extends DeesElement {
  // STATIC
  public static demo = () => html`<dees-windowlayer></dees-windowlayer>`;

  // INSTANCE
  @property({
    type: Boolean
  })
  public visible = false;

  constructor() {
    super();
    domtools.elementBasic.setup();
  }

  public render(): TemplateResult {
    return html`
      ${domtools.elementBasic.styles}
      <style>
        .windowOverlay {
          transition: all 0.3s;
          will-change: transform;
          position: fixed;
          top: 0px;
          left: 0px;
          height: 100vh;
          width: 100vw;
          display: flex;
          justify-content: center;
          align-items: center;
          background: rgba(0, 0, 0, 0.0);
          backdrop-filter: brightness(1);
          pointer-events: none;
          z-index: 200;
        }
        
        .visible {
          background: rgba(0, 0, 0, 0.2);
          backdrop-filter: brightness(0.3);
          pointer-events: all;
        }
      </style>
      <div @click=${this.dispatchClicked} class="windowOverlay ${this.visible ? 'visible' : null}">
        <slot></slot>
      </div>
    `;
  }

  firstUpdated() {
    setTimeout(() => {
      this.visible = true;
    }, 100);
  }

  dispatchClicked() {
    this.dispatchEvent(new CustomEvent('clicked'));
  }

  public toggleVisibility () {
    this.visible = !this.visible;
  }

  public async show() {
    const domtools = await this.domtoolsPromise;
    await domtools.convenience.smartdelay.delayFor(0);
    this.visible = true;
  }

  public async hide() {
    const domtools = await this.domtoolsPromise;
    await domtools.convenience.smartdelay.delayFor(0);
    this.visible = false;
  }
}
