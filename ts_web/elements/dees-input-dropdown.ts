import { customElement, DeesElement, type TemplateResult, property, html, css, cssManager, type CSSResult, } from '@design.estate/dees-element';
import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-input-dropdown': DeesInputDropdown;
  }
}

@customElement('dees-input-dropdown')
export class DeesInputDropdown extends DeesElement {
  public static demo = () => html`
    <dees-input-dropdown
      .options=${[
        {option: 'option 1', key: 'option1'},
        {option: 'option 2', key: 'option2'},
        {option: 'option 3', key: 'option3'}
      ]}
    ></dees-input-dropdown>
  `

  // INSTANCE
  public changeSubject = new domtools.rxjs.Subject();

  @property()
  public label: string = 'Label';

  @property()
  public key: string;

  @property()
  public options: {option: string, key: string, payload?: any}[] = [];

  @property()
  public selectedOption: {option: string, key: string, payload?: any} = {
    key: null,
    option: null,
    payload: null
  };

  @property({
    type: Boolean
  })
  public required: boolean = false;

  @property({
    type: Boolean
  })
  public disabled: boolean = false;

  public static styles = [
    cssManager.defaultStyles,
    css`
      * {
          box-sizing: border-box;
        }

        :host {
          position: relative;
          display: block;
          height: 40px;
          color: ${cssManager.bdTheme('#222', '#fff')};
        }

        .maincontainer {
          display: block;
        }

        .label {
          font-size: 14px;
          margin-bottom: 15px;
        }

        .selectedBox {
          cursor: pointer;
          position: relative;
          max-width: 420px;
          height: 40px;
          line-height: 40px;
          padding: 0px 8px;
          z-index: 0px;
          background: ${cssManager.bdTheme('#ffffff', '#333333')};
          box-shadow: ${cssManager.bdTheme('0px 1px 4px rgba(0,0,0,0.3)', 'none')};
          border-radius: 3px;
          border-top: 1px solid #CCCCCC00;
          border-bottom: 1px solid #66666600;
        }

        .selectedBox.show {
          border-top: 1px solid ${cssManager.bdTheme('#ffffff', '#666666')};
          border-bottom: 1px solid ${cssManager.bdTheme('#fafafa', '#222222')};
        }

        .selectionBox {
          will-change:transform;
          pointer-events: none;
          cursor: pointer;
          transition: all 0.2s ease;
          opacity: 0;
          position: relative;
          background: ${cssManager.bdTheme('#ffffff', '#222222')};
          max-width: 420px;
          box-shadow: 0px 0px 5px  rgba(0,0,0,0.2);
          min-height: 40px;
          margin-top: -40px;
          z-index: 100;
          border-radius: 3px;
          padding: 4px;
          transform: scale(0.99,0.99);
        }

        .selectionBox.show {
          pointer-events: all;
          opacity: 1;
          transform: scale(1,1);
        }

        .option {
          transition: all 0.1s;
          line-height: 40px;
          padding: 0px 4px;
          border-radius: 3px;
        }

        .option:hover {
          color: #fff;
          padding-left: 8px;
          background: #0277bd;
        }
    `
  ]

  public render(): TemplateResult {
    return html`
      ${domtools.elementBasic.styles}
      <style>
        
      </style>
      <div class="maincontainer">
        <div class="selectedBox show" @click="${event => {this.toggleSelectionBox();}}">
          ${this.selectedOption?.option}
        </div>
        <div class="selectionBox">
          ${this.options.map(option => {
            return html`
              <div class="option" @click=${() => {this.updateSelection(option);}}>${option.option}</div>
            `
          })}
        </div>
      </div>
    `;
  }

  firstUpdated() {
    this.selectedOption = this.options[0] || null;
  }

  public async updateSelection(selectedOption) {
    this.selectedOption = selectedOption;

    this.dispatchEvent(new CustomEvent('selectedOption', {
      detail: selectedOption,
      bubbles: true
    }));
    this.toggleSelectionBox();
    this.changeSubject.next(this);
  }

  public toggleSelectionBox() {
    this.shadowRoot.querySelector('.selectedBox').classList.toggle('show');
    this.shadowRoot.querySelector('.selectionBox').classList.toggle('show');
  }
}
