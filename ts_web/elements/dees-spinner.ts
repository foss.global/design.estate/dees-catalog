import {
  customElement,
  html,
  DeesElement,
  property,
  type TemplateResult,
  cssManager,
  css,
  type CSSResult,
  unsafeCSS,
} from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-spinner': DeesSpinner;
  }
}

@customElement('dees-spinner')
export class DeesSpinner extends DeesElement {
  public static demo = () => html`
    <dees-spinner></dees-spinner>
    <dees-spinner status="success"></dees-spinner>
    <dees-spinner status="error"></dees-spinner>
    <dees-spinner size=${64} status="success"></dees-spinner>
    <dees-spinner .size=${64} status="error"></dees-spinner>
  `;

  @property({
    type: Number,
  })
  public size = 20;

  @property({
    type: String,
  })
  public bnw: boolean = false;

  @property()
  public status: 'normal' | 'pending' | 'success' | 'error' = 'normal';

  constructor() {
    super();
  }

  public static styles = [
    cssManager.defaultStyles,
    css`
      :host {
        display: block;
      }

      #loading {
        position: relative;
        transition: none;
        display: flex;
        justify-content: center;
        align-content: center;
        background: #8bc34a00;
        border: 3px solid ${cssManager.bdTheme('rgba(0, 0, 0, 0.1)', 'rgba(255, 255, 255, 0.3)')};
        border-radius: 50%;
        border-top-color: ${cssManager.bdTheme('#333', '#fff')};
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite;
      }

      #loading.success {
        border: none;
        border-radius: 50%;
        animation: none;
        -webkit-animation: none;
      }

      #loading.error {
        border: none;
        border-radius: 50%;
        animation: none;
        -webkit-animation: none;
      }

      @keyframes spin {
        to {
          -webkit-transform: rotate(360deg);
        }
      }
      @-webkit-keyframes spin {
        to {
          -webkit-transform: rotate(360deg);
        }
      }

      dees-icon {
        position: absolute;
        height: 100%;
        width: 100%;
      }
    `,
  ];

  render() {
    return html`
      <style>
        #loading {
          width: ${this.size}px;
          height: ${this.size}px;
        }
        #loading.success {
          color: ${cssManager.bdTheme(this.bnw ? '#333': `#8bc34a`, this.bnw ? '#fff' : `#8bc34a`)};
          
        }
        #loading.error {
          color: ${cssManager.bdTheme(this.bnw ? '#333': `#e64a19`, this.bnw ? '#fff' : `#e64a19`)};
        }
        dees-icon {
          font-size: ${this.size}px;
        }
      </style>
      <div class="${this.status}" id="loading">
        ${(() => {
          if (this.status === 'success') {
            return html`<dees-icon style="transform: translateX(1%) translateY(3%);" .iconFA=${'circleCheck' as any}></dees-icon>`;
          } else if (this.status === 'error') {
            return  html`<dees-icon .iconFA=${'circleXmark' as any}></dees-icon>`;
          }
        })()}
      </div>
    `;
  }
}
