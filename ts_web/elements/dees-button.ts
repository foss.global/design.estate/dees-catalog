import {
  customElement,
  html,
  DeesElement,
  property,
  type TemplateResult,
  cssManager,
  css,
  type CSSResult,
  unsafeCSS,
} from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-button': DeesButton;
  }
}

@customElement('dees-button')
export class DeesButton extends DeesElement {
  public static demo = () => html`
    <dees-button>This is a slotted Text</dees-button>
    <p><dees-button text="Highlighted: This text shows" type="highlighted">Highlighted</dees-button></p>
    <p><dees-button type="discreet">This is discreete button</dees-button></p>
    <p><dees-button disabled>This is a disabled button</dees-button></p>
    <p><dees-button type="big">This is a slotted Text</dees-button></p>
    <p><dees-button status="normal">Normal Status</dees-button></p>
    <p><dees-button disabled status="pending">Pending Status</dees-button></p>
    <p><dees-button disabled status="success">Success Status</dees-button></p>
    <p><dees-button disabled status="error">Error Status</dees-button></p>
  `;

  @property({
    reflect: true,
    hasChanged() {
      return true;
    }
  })
  public text: string;

  @property()
  public eventDetailData: string;

  @property({
    type: Boolean
  })
  public disabled = false;

  @property({
    type: Boolean
  })
  public isHidden = false;

  @property({
    type: String
  })
  public type: 'normal' | 'highlighted' | 'discreet' | 'big' = 'normal';

  @property({
    type: String
  })
  public status: 'normal' | 'pending' | 'success' | 'error' = 'normal';

  constructor() {
    super();
  }

  public static styles = [
    cssManager.defaultStyles,
    css`

      :host {
        display: block;
        box-sizing: border-box;
      }
      :host([hidden]) {
        display: none;
      }

      .button {
        transition: all 0.1s , color 0s;
        position: relative;
        font-size: 14px;
        font-weight: 400;
        display: flex;
        justify-content: center;
        align-items: center;
        background: ${cssManager.bdTheme('#fff', '#333')};
        box-shadow: ${cssManager.bdTheme('0px 1px 3px rgba(0,0,0,0.3)', 'none')};
        border: 1px solid ${cssManager.bdTheme('#eee', '#333')};
        border-top: ${cssManager.bdTheme('1px solid #eee', '1px solid #444')};
        border-radius: 4px;
        line-height: 40px;
        padding: 0px 8px;
        min-width: 100px;
        user-select: none;
        color: ${cssManager.bdTheme('#333', ' #ccc')};
        max-width: 500px;
      }

      .button:hover {
        cursor: pointer;
        background: #039be5;
        color: #ffffff;
        border: 1px solid #039be5;
        border-top: 1px solid #039be5;
      }

      .button:active {
        background: #0277bd;
        border-top: 1px solid #0277bd;
      }

      .button.disabled {
        background: ${cssManager.bdTheme('#ffffff00', '#11111100')};
        border: 1px dashed ${cssManager.bdTheme('#666666', '#666666')};
        color: #9b9b9e;
        cursor: default;
      }

      .button.highlighted {
        background: #e4002b;
        border: none;
        color: #fff;
      }

      .button.highlighted:hover {
        background: #b50021;
        border: none;
        color: #fff;
      }

      .button.discreet {
        background: none;
        border: 1px solid #9b9b9e;
        color: ${cssManager.bdTheme('#000', '#fff')};
      }

      .button.discreet:hover {
        background: ${cssManager.bdTheme('rgba(0, 0, 0, 0.1)', 'rgba(255, 255, 255, 0.1)')};
      }
      .button.hidden {
        display: none;
      }

      .button.big {
        width: 300px;
        line-height: 48px;
        font-size: 16px;
        padding: 0px 48px;
        margin-top: 32px;
      }

      .button.pending {
        border: 1px dashed ${cssManager.bdTheme('#0277bd', '#0277bd70')};
        background: ${cssManager.bdTheme('#0277bd', '#0277bd70')};
        color: #fff;
      }

      .button.success {
        border: 1px dashed ${cssManager.bdTheme('#689F38', '#8BC34A70')};
        background: ${cssManager.bdTheme('#689F38', '#8BC34A70')};
        color: #fff;
      }

      .button.error {
        border: 1px dashed ${cssManager.bdTheme('#B71C1C', '#E64A1970')};
        background: ${cssManager.bdTheme('#B71C1C', '#E64A1970')};
        color: #fff;
      }

      dees-spinner {
        position: absolute;
        left: 10px;
      }
    `,
  ];

  public render(): TemplateResult {
    return html`
      <div
        class="button ${this.isHidden ? 'hidden' : 'block'}  ${this.type} ${this.status} ${this.disabled
          ? 'disabled'
          : null}"
        @click="${this.dispatchClick}"
      >
        ${this.status === 'normal' ? html``: html`
          <dees-spinner .bnw=${true} status="${this.status}"></dees-spinner>
        `}
        <div class="textbox">${this.text ? this.text : this.textContent}</div>
      </div>
    `;
  }

  public async dispatchClick() {
    if (this.disabled) {
      return;
    }

    this.dispatchEvent(
      new CustomEvent('clicked', {
        detail: {
          data: this.eventDetailData,
        },
        bubbles: true,
      })
    );
  }

  public async firstUpdated() {
    if (!this.textContent) {
      this.textContent = 'Button';
      this.performUpdate();
    }
  }
}
