import {
  DeesElement,
  html,
  property,
  customElement,
  cssManager,
  css,
  type CSSResult,
} from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';

import { icon, type IconDefinition } from '@fortawesome/fontawesome-svg-core';
import {
  faFacebook,
  faGoogle,
  faLinkedin,
  faMedium,
  faSlackHash,
  faTwitter,
  faInstagram,
  faTiktok,
} from '@fortawesome/free-brands-svg-icons';

import {
  faCopy as faCopyRegular,
  faCircleCheck as faCircleCheckRegular,
  faCircleXmark as faCircleXmarkRegular,
  faMessage as faMessageRegular,
  faPaste as faPasteRegular,
  faSun as faSunRegular,
} from '@fortawesome/free-regular-svg-icons';
import {
  faArrowRight as faArrowRightSolid,
  faArrowUpRightFromSquare as faArrowUpRightFromSquareSolid,
  faBell as faBellSolid,
  faBug as faBugSolid,
  faBuilding as faBuildingSolid,
  faCaretLeft as faCaretLeftSolid,
  faCaretRight as faCaretRightSolid,
  faCheck as faCheckSolid,
  faCircleInfo as faCircleInfoSolid,
  faCircleCheck as faCircleCheckSolid,
  faCircleXmark as faCircleXmarkSolid,
  faCopy as faCopySolid,
  faDesktop as faDesktopSolid,
  faEye as faEyeSolid,
  faEyeSlash as faEyeSlashSolid,
  faGrip as faGripSolid,
  faMessage as faMessageSolid,
  faMugHot as faMugHotSolid,
  faMinus as faMinusSolid,
  faPaste as faPasteSolid,
  faPenToSquare as faPenToSquareSolid,
  faRss as faRssSolid,
  faUsers as faUsersSolid,
  faShare as faShareSolid,
  faSun as faSunSolid,
  faXmark as faXmarkSolid,
} from '@fortawesome/free-solid-svg-icons';

export const faIcons = {
  // normal
  arrowRight: faArrowRightSolid,
  arrowUpRightFromSquare: faArrowUpRightFromSquareSolid,
  arrowUpRightFromSquareSolid: faArrowUpRightFromSquareSolid,
  bell: faBellSolid,
  bellSolid: faBellSolid,
  bug: faBugSolid,
  bugSolid: faBugSolid,
  building: faBuildingSolid,
  buildingSolid: faBuildingSolid,
  caretLeft: faCaretLeftSolid,
  caretLeftSolid: faCaretLeftSolid,
  caretRight: faCaretRightSolid,
  caretRightSolid: faCaretRightSolid,
  check: faCheckSolid,
  checkSolid: faCheckSolid,
  circleInfo: faCircleInfoSolid,
  circleInfoSolid: faCircleInfoSolid,
  circleCheck: faCircleCheckRegular,
  circleCheckSolid: faCircleCheckSolid,
  circleXmark: faCircleXmarkRegular,
  circleXmarkSolid: faCircleXmarkSolid,
  copy: faCopyRegular,
  copySolid: faCopySolid,
  desktop: faDesktopSolid,
  desktopSolid: faDesktopSolid,
  eye: faEyeSolid,
  eyeSolid: faEyeSolid,
  eyeSlash: faEyeSlashSolid,
  eyeSlashSolid: faEyeSlashSolid,
  grip: faGripSolid,
  gripSolid: faGripSolid,
  message: faMessageRegular,
  messageSolid: faMessageSolid,
  mugHot: faMugHotSolid,
  faMugHotSolid: faMugHotSolid,
  minus: faMinusSolid,
  minusSolid: faMinusSolid,
  paste: faPasteRegular,
  pasteSolid: faPasteSolid,
  penToSquare: faPenToSquareSolid,
  penToSquareSolid: faPenToSquareSolid,
  rss: faRssSolid,
  rssSolid: faRssSolid,
  share: faShareSolid,
  shareSolid: faShareSolid,
  sun: faSunRegular,
  sunSolid: faSunSolid,
  xmark: faXmarkSolid,
  xmarkSolid: faXmarkSolid,
  // brands
  facebook: faFacebook,
  google: faGoogle,
  instagram: faInstagram,
  linkedin: faLinkedin,
  medium: faMedium,
  slack: faSlackHash,
  tiktok: faTiktok,
  twitter: faTwitter,
  users: faUsersSolid,
};

declare global {
  interface HTMLElementTagNameMap {
    'dees-icon': DeesIcon;
  }
}

@customElement('dees-icon')
export class DeesIcon extends DeesElement {
  public static demo = () => html`
    <dees-icon iconName="visibility"></dees-icon>
    <div style="background: #fff; padding: 10px; font-size: 30px">
      <style>
        dees-icon {
          transition: color 0.05s;
        }
        dees-icon:hover {
          color: #e4002b;
        }
      </style>
      <dees-icon .iconFA=${'messageSolid'}></dees-icon>
      <dees-icon .iconFA=${'sun'}></dees-icon>
      <dees-icon .iconFA=${'sunSolid'}></dees-icon>
      <dees-icon .iconFA=${'facebook'}></dees-icon>
      <dees-icon .iconFA=${'arrowUpRightFromSquare'}></dees-icon>
    </div>
  `;

  @property({
    type: String
  })
  public iconFA: keyof typeof faIcons;

  @property()
  public iconSize: number;

  constructor() {
    super();
    domtools.elementBasic.setup();
  }

  public static styles = [
    cssManager.defaultStyles,
    css`
      :host {
        display: block;
        white-space: nowrap;
        display: flex;
        align-items: center;
        justify-content: center;
      }
      * {
        transition: inherit !important;
      }
    `,
  ];

  public render() {
    return html`
      ${domtools.elementBasic.styles}
      <style>
        #iconContainer svg {
          display: block;
          height: ${this.iconSize}px;
        }
      </style>
      <div id="iconContainer"></div>
    `;
  }

  public async updated() {
    if (!this.iconSize) {
      this.iconSize = parseInt(globalThis.getComputedStyle(this).fontSize.replace(/\D/g,''));
    }
    if (this.iconFA) {
      this.shadowRoot.querySelector('#iconContainer').innerHTML = this.iconFA
        ? icon(faIcons[this.iconFA]).html[0]
        : 'icon not found';
    }
  }
}
