import { customElement, DeesElement, type TemplateResult, html, property, type CSSResult, } from '@design.estate/dees-element';

import * as domtools from '@design.estate/dees-domtools';


import './dees-windowlayer';
import { css, cssManager } from '@design.estate/dees-element';

declare global {
  interface HTMLElementTagNameMap {
    'dees-updater': DeesUpdater;
  }
}

@customElement('dees-updater')
export class DeesUpdater extends DeesElement {
  public static demo = () => html`<dees-updater></dees-updater>`;

  @property({
    type: String,
  })
  currentVersion: string;

  @property({
    type: String,
  })
  updatedVersion: string;

  constructor() {
    super();
    domtools.elementBasic.setup();
  }

  public static styles = [
    cssManager.defaultStyles,
    css`
      .modalContainer {
          will-change: transform;
          position: relative;
          background: ${cssManager.bdTheme('#eeeeeb', '#222')};
          margin: auto;
          max-width: 800px;
          border-radius: 3px;
          border-top: 1px solid ${cssManager.bdTheme('#eeeeeb', '#333')};
        }

        .headingContainer {
          display: flex;
          justify-content: center;
          align-items: center;
          padding: 40px 40px;
        }

        h1 {
          margin: none;
          font-size: 20px;
          color: ${cssManager.bdTheme('#333', '#fff')};
          margin-left: 20px;
          font-weight: normal;
        }

        .buttonContainer {
          display: grid;
          grid-template-columns: 50% 50%;
        }
    `
  ]

  public render(): TemplateResult {
    return html`
      <dees-windowlayer @clicked="${this.windowLayerClicked}">
        <div class="modalContainer">
          <div class="headingContainer">
            <dees-spinner .size=${60}></dees-spinner>
            <h1>Updating the application...</h1>
          </div>
          <div class="buttonContainer">
            <dees-button>More info</dees-button>
            <dees-button>Changelog</dees-button>
          </div>
        </div>
      </dees-windowlayer>>
    `;
  }

  private windowLayerClicked() {
    const windowLayer = this.shadowRoot.querySelector('dees-windowlayer');
    windowLayer.toggleVisibility();
  }
}
