import {customElement, DeesElement, type TemplateResult, property, html, cssManager, type CSSResult,} from '@design.estate/dees-element';
import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-input-text': DeesInputText;
  }
}

@customElement('dees-input-text')
export class DeesInputText extends DeesElement {
  public static demo = () => html`
    <dees-input-text .label=${'this is a label'} .value=${'test'}></dees-input-text>
    <dees-input-text .isPasswordBool=${true}></dees-input-text>
  `;

  // INSTANCE
  public changeSubject = new domtools.rxjs.Subject<DeesInputText>();
  public valueChangeSubject = new domtools.rxjs.Subject<string>();

  @property({
    type: String
  })
  public label: string;

  @property({
    type: String
  })
  public key: string;

  @property({
    type: String,
    reflect: true,
  })
  public value: string = '';

  @property({
    type: Boolean
  })
  public required: boolean = false;

  @property({
    type: Boolean
  })
  public disabled: boolean = false;

  @property({
    type: Boolean,
    reflect: true,
  })
  public isPasswordBool = false;

  @property({
    type: Boolean,
    reflect: true,
  })
  public showPasswordBool = false;

  public render(): TemplateResult {
    return html `
      <style>
        * {
          box-sizing: border-box;
        }

        :host {
          position: relative;
          display: grid;
          margin: 10px 0px;
          margin-bottom: 24px;
        }

        .maincontainer {
          color: ${this.goBright ? '#333' : '#ccc'};
        }

        .label {
          font-size: 14px;
          margin-bottom: 5px;
        }

        input {
          margin-top: 5px;
          background: ${this.goBright ? '#fafafa' : '#222'};
          border-top: ${this.goBright ? '1px solid #CCC'  : '1px solid #444'};
          border-bottom: ${this.goBright ? '1px solid #CCC' : '1px solid #333'};
          border-right: ${this.goBright ? '1px solid #CCC' : 'none'};
          border-left: ${this.goBright ? '1px solid #CCC' : 'none'};
          padding-left:10px;
          padding-right: 10px;
          border-radius: 2px;
          width: 100%;
          line-height: 48px;
          transition: all 0.2s;
          outline: none;
          font-size: 16px;
          font-family: ${this.isPasswordBool ? 'monospace': 'Inter'};
          letter-spacing: ${this.isPasswordBool ? '1px': 'normal'};
          color: ${this.goBright ? '#333' : '#ccc'};
        }

        input:disabled {
          background: ${cssManager.bdTheme('#ffffff00', '#11111100')};
          border: 1px dashed ${cssManager.bdTheme('#666666', '#666666')};
          color: #9b9b9e;
          cursor: default;
        }

        input:focus {
          outline: none;
          border-bottom: 1px solid #e4002b;
        }

        .showPassword {
          position: absolute;
          bottom: 8px;
          right: 10px;
          border: 1px dashed #444;
          border-radius: 7px;
          padding: 8px 0px;
          width: 40px;
        }

        .showPassword:hover {
          cursor: pointer;
          background: #333;
        }
      </style>
      <div class="maincontainer">
        ${this.label ? html`<div class="label">${this.label}</div>` : html``}
        <input type="${this.isPasswordBool && !this.showPasswordBool ? 'password' : 'text'}" value=${this.value} @input="${this.updateValue}" .disabled=${this.disabled} />
        ${this.isPasswordBool ? html`
          <div class="showPassword" @click=${this.togglePasswordView}>
            <dees-icon .iconFA=${this.showPasswordBool ? 'eye' : 'eyeSlash'}></dees-icon>
          </div>
        ` : html``}
      </div>
    `;
  }

  public async updateValue(eventArg: Event) {
    const target: any = eventArg.target;
    this.value = target.value;
    this.changeSubject.next(this);
    this.valueChangeSubject.next(this.value);
  }

  public async freeze() {
    this.disabled = true;
  }

  public async unfreeze() {
    this.disabled = false;
  }

  public async togglePasswordView () {
    const domtools = await this.domtoolsPromise;
    this.showPasswordBool = !this.showPasswordBool;
    console.log(`this.showPasswordBool is: ${this.showPasswordBool}`)
  }

  public async focus() {
    const textInput = this.shadowRoot.querySelector('input');
    textInput.focus();
  }

  public async blur() {
    const textInput = this.shadowRoot.querySelector('input');
    textInput.blur();
  }
}
