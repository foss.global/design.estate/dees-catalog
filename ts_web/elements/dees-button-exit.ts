import {
  cssManager,
  customElement,
  DeesElement,
  html,
  type TemplateResult,
  css,
  type CSSResult,
  state,
  property
} from '@design.estate/dees-element';

@customElement('dees-button-exit')
export class DeesButtonExit extends DeesElement {
  // DEMO
  public static demo = () => html`
    <dees-button-exit></dees-button-exit>
  `;

  // INSTANCE
  @property({
    type: Number
  })
  public size: number = 24;

  public styles = [
    cssManager.defaultStyles,
    css`

    `
  ]

  public render (): TemplateResult {
    return html`
      <style>
        .maincontainer {
          position: relative;
          width: ${this.size}px;
          height: ${this.size}px;
        }
      </style>
      <div class="maincontainer">
        <div class="firstLine"></div>
        <div class="secondLine"></div>
      </div>
    `;
  }
}