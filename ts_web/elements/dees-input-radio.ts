import {customElement, DeesElement, type TemplateResult, property, html, type CSSResult,} from '@design.estate/dees-element';
import * as domtools from '@design.estate/dees-domtools';

declare global {
  interface HTMLElementTagNameMap {
    'dees-input-radio': DeesInputRadio;
  }
}

@customElement('dees-input-radio')
export class DeesInputRadio extends DeesElement {
  public static demo = () => html`<dees-input-radio></dees-input-radio>`;

  // INSTANCE
  public changeSubject = new domtools.rxjs.Subject();

  @property()
  public key: string;

  @property()
  public label: string = 'Label';

  @property()
  public value: boolean = false;

  @property({
    type: Boolean,
  })
  public required: boolean = false;

  @property({
    type: Boolean
  })
  public disabled: boolean = false;

  constructor() {
    super();
  }

  public render(): TemplateResult {
    return html `
      <style>
        * {
          box-sizing: border-box;
        }

        :host {
          display: block;
          position: relative;
          margin: 20px 0px;
          cursor: pointer;
        }

        .maincontainer {
          transition: all 0.3s;
          display: grid;
          grid-template-columns: 25px auto;
          padding: 5px 0px;
          color: #ccc;
        }

        .maincontainer:hover {
          color: #fff;
        }

        .label {
          margin-left: 15px;
          line-height: 25px;
          font-size: 14px;
          font-weight: normal;

        }

        input:focus {
          outline: none;
          border-bottom: 1px solid #e4002b;
        }

        .checkbox {
          transition: all 0.3s;
          box-sizing: border-box;
          border-radius: 20px;
          border: 1px solid #999;
          height: 24px;
          width: 24px;
          display: inline-block;
          background: #222;
        }

        .checkbox.selected {
          background: #039BE5;
          border: 1px solid #039BE5;
        }

        .maincontainer:hover .checkbox.selected {
          background: #03A9F4;
        }

        .innercircle {
          transition: all 0.3s;
          margin: 6px 0px 0px 6px;
          background: #222;
          width: 10px;
          height: 10px;
          border-radius: 10px;
        }
      </style>
      <div class="maincontainer" @click="${this.toggleSelected}">
        <div class="checkbox ${this.value ? 'selected' : ''}">
        ${this.value ? html`<div class="innercircle"></div>`: html``}
        </div>
        <div class="label">${this.label}</div>
      </div>
    `;
  }

  public async toggleSelected () {
    this.value = !this.value;
    this.dispatchEvent(new CustomEvent('newValue', {
      detail: this.value,
      bubbles: true
    }));
    this.changeSubject.next(this);
  }
}