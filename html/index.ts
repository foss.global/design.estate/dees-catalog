// dees tools
import * as deesWccTools from '@designestate/dees-wcctools';
import * as deesDomTools from '@designestate/dees-domtools';

// elements and pages
import * as elements from '../ts_web/elements/index.js';
import * as pages from '../ts_web/pages/index.js';

deesWccTools.setupWccTools(elements as any, pages);
deesDomTools.elementBasic.setup();
